---
- name: Install mandatory packages for installation
  package:
    name:
      - unzip
      - gnupg
      - perl-Digest-SHA
    state: present

- name: Create system group
  group:
    name: "{{ vault_user_name }}"
    gid: "{{ vault_user_id }}"
    system: yes

- name: Create system user
  user:
    name: "{{ vault_user_name }}"
    group: "{{ vault_user_name }}"
    uid: "{{ vault_user_id }}"
    home: "{{ vault_config_dir }}"
    shell: "/bin/false"
    createhome: no
    system: yes

- name: Create config directory
  file:
    path: "{{ item }}"
    owner: "{{ vault_user_name }}"
    group: "{{ vault_user_name }}"
    state: directory
    mode: 0750
  loop:
    - "{{ vault_config_dir }}"
    - "{{ vault_agent_config_dir }}"

- name: Create cache directory for download
  file:
    path: "{{ vault_ansible_cache_dir }}"
    state: directory
    mode: 0750

- name: Copy Hashicorp public gpg key
  copy:
    src: "hashicorp.asc"
    dest: "{{ vault_ansible_cache_dir }}/hashicorp.asc"
  notify: Import Hashicorp public gpg key

- meta: flush_handlers

- name: Download vault {{ vault_version }} sha256sums
  get_url:
    url: "{{ vault_sha256sums_url }}"
    dest: "{{ vault_sha256sums_dest }}"
  notify: Verify vault sha256sums signature

- name: Download vault {{ vault_version }} sha256sums signature
  get_url:
    url: "{{ vault_sha256sums_sig_url }}"
    dest: "{{ vault_sha256sums_sig_dest }}"
  notify: Verify vault sha256sums signature

- meta: flush_handlers

- name: Download vault {{ vault_version }} release
  get_url:
    url: "{{ vault_archive_url }}"
    dest: "{{ vault_archive_dest }}"
  notify: Verify vault release hash

- meta: flush_handlers

- name: Unarchive vault {{ vault_version }} release
  unarchive:
    src: "{{ vault_archive_dest }}"
    dest: "{{ vault_bin_dir }}"
    creates: "{{ vault_bin_path }}"
    remote_src: yes
  notify: Give vault mlock syscall (prevents memory disk swap)

- name: Set rights on vault binary
  file:
    path: "{{ vault_bin_path }}"
    owner: "root"
    group: "{{ vault_user_name }}"
    mode: 0750

- name: Create vault system services
  template:
    src: "{{ item.src }}"
    dest: "/lib/systemd/system/{{ item.dest }}"
    mode: 0640
  loop:
    - { src: "vault.service.j2", dest: "vault.service" }
    - { src: "vault-agent.service.j2", dest: "vault-agent.service" }
  notify: Restart vault service

- name: Create rsyslog configuration
  copy:
    dest: "/etc/rsyslog.d/vault.conf"
    mode: 0640
    content: |-
      :programname, isequal, "vault" {{ vault_log_dest }}
      & stop
  notify: Restart rsyslog service

- name: Create local facts
  copy:
    dest: "/etc/ansible/facts.d/vault.fact"
    content: >-
      {
        "config_dir": "{{ vault_config_dir }}",
        "agent_config_dir": "{{ vault_agent_config_dir }}",
        "version": "{{ vault_version }}",
        "log_dest": "{{ vault_log_dest }}"
      }
